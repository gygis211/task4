class Elements {
    constructor(className, html) {
        this.html = html;
        this.className = className;
    }

    render() {
        const element = document.querySelector(this.className);
        if (element) {
            element.innerHTML = this.html
        }
    }

    delete() {
        const element = document.querySelector(this.className);
        element.remove();
    }
}

class Images extends Elements {
    constructor(className, src) {
        super(className);
        this.src = src;
    }

    imgRender() {
        const parentBlock = document.querySelector(this.className);
        const image = document.createElement("img");
        image.src = this.src;
        parentBlock.append(image);
    }
}

const newImg = new Images('.userPhoto', 'https://sun9-35.userapi.com/impf/j3YPiZ0QEVluC6RJKV2rXM6XBJTpkQNtaA4dpw/4lx205tu18A.jpg?size=765x1024&quality=96&sign=13eea4b5b5924de7067e9f7736fa8c96&type=album');
newImg.imgRender();

const newContacts = new Elements('.side1', `
<h2>Контактные данные </h2>
<ul>
<nobr>
    <li>Адрес:</li>
    <li>г.Минск</li>
    <li>ул.Якуба Коласа</li>
    <li>Электронная почта:</li>
    <li>gygis211@gmail.com</li>
    <li>Портфолио:</li>
    <li>скоро появится...</li>
</nobr>
</ul>`);
newContacts.render();

const newRecomendation = new Elements('.side2', `
<h2>Рекомендации:</h2>
<ul>
	<nobr>
		<li>https://vironit.com/</li>
	</nobr>
</ul>`);
newRecomendation.render();

const newScils = new Elements('.side3', `
<h2>Ключевые навыки:</h2>
<ul>
	<nobr>
		<li>HTML</li>
		<li>CSS</li>
		<li>JS</li>
		<li>WordPress</li>
		<li>MySQL</li>
		<li>Интернет-грамотность</li>
		<li>>Английский язык – B2</li>
	</nobr>
</ul>`);
newScils.render();

const newContent = new Elements('.main', `
<button id="themeButton" class="button_1">Сменить тему</button>
<div class="maincontent"></div>
`);
newContent.render();

const maincontent = new Elements('.maincontent', `
<h2>Дмитрий Кульпанович</h2>
<h3>JS разработчик</h3>
<br>
<hr>
<h3>Опыт работы:</h3>
<div class="form form1"></div>
<div class="form form2"></div>
<br>
<hr>
<h3>Образование:</h3>

<div>
<h4>БНТУ</h4>
<h4>2015 – 2017</h4>
(заочная форма обучения)
Экономика и организация производства (энергетика)
</div>
<br>
<hr>
<div>
<div class="editTextBox" contenteditable="false">
<h3>О себе:</h3>

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
</div>
<button class="button_2">Редактировать</button>
</div>
<div class="info" id="parent">
<button class="addButton" id="button2"
onclick="newFunc('#parent', 'someButton', 'someField', id='button')">Добавить
информацию</button>
</div>
`);
maincontent.render();

const form1 = new Elements('.form1', `
<h4>Frontend Developer</h4>
					<h4>ИП АЛИПОВ АНДРЕЙ ВЛАДИМИРОВИЧ</h4>
					Сентябрь 2018 – настоящее время
					<ul>
						<li>Вёрстка сайтов на WordPress</li>
						<li>Создание сайтов и веб-страниц (HTML, CSS, JS)</li>
						<li>Поиск решений для удобного пользовательского интерфейса</li>
						<li>Поиск и исправление "багов"</li>
						<li>Техническая поддержка сайтов клиентов (наполнение контентом, разработка дополнительных
							сервисов на основе PHP)</li>
						<br>
					</ul>
`);
form1.render();

const form2 = new Elements('.form2', `
<h4>Frontend Developer</h4>
					<h4>ИП АЛИПОВ АНДРЕЙ ВЛАДИМИРОВИЧ</h4>
					Июнь 2020 – настоящее время
					<ul>
						<li>Создание логотипов, айдентики, иконок, баннеров и веб-элементов</li>
						<li>Архитектура и вёрстка дизайнов сайтов</li>
						<li>Ретушь и коллаж изображений</li>
						<li>Индивидуальное и командное ведение проектов</li>
						<br>
					</ul>
`);
form2.render();

let sidebarId = document.querySelector('#sidebarId'),
    mainId = document.querySelector('#mainId'),
    btnTheme = document.querySelector('#themeButton'),

    btnEdit = document.querySelector('.button_2'),
    editTextBox = document.querySelector('.editTextBox')

btnTheme.onclick = () => {
    sidebarId.classList.toggle('sidebar-light');
    mainId.classList.toggle('main-light');
};

let flag = true;
btnEdit.onclick = () => { 
    if (flag) {
        flag = false;
        editTextBox.contentEditable = "true";
        btnEdit.textContent = "Сохранить"
    } else {
        flag = true;
        editTextBox.contentEditable = "false";
        btnEdit.textContent = "Редактировать"
    }
    editTextBox.classList.toggle('editBox');
};

let getMainContent = document.querySelector('.maincontent')
let addInfoBtn = document.querySelector('.buttonAddInfo')

const newFunc = (parentId, buttonId, noteClass, thisButtonId) =>{
    document.querySelector(thisButtonId).remove()
    const parent = document.querySelector(parentId)
    const note = createField(parent, noteClass)
    const button = createButton(parent, buttonId)
    added = true
    newButtonAction(button, note)
    console.log(parent.id)
}

const newButtonAction = (button, note) => {
    const newButton = document.getElementById(button.id)
    let flag = true
    newButton.addEventListener("click", ()=>{
        if(flag === true){
            button.textContent = "Сохранить"
            note.contentEditable = true
            note.focus()
            flag = false
        }else{
            button.textContent = "Редактировать"
            note.contentEditable = false
            flag = true
        }
    })
    return true
}

const createField = (parent, className) => {
    const newP = document.createElement('div')
    newP.className = className
    newP.innerHTML = 'New text field'
    parent.appendChild(newP);
    return newP
}


const createButton = (parent, id) =>{
    const newButton = document.createElement('button')
    newButton.id = id
    newButton.textContent = 'Редактировать'
    parent.appendChild(newButton);
    return newButton
}